INSTALLATION :

- Copier le dossier dans le répertoire des sites Apache (www/)
- Activer le Url Rewriting dans les configs Apache (décommenter LoadModule rewrite_module dans httpd.conf)
- Lancer MySQL
- Créer la base de données kaizamada (CREATE DATABASE kaizamada)
- importer la base depuis le fichier base.sql
- ouvrir le fichier .htaccess et modifier le chemin absolu du fichier .htpasswd (lignes 13 et 19)
- modifier user/mot de passe dans application/config/database.php si nécessaire.
- Lancer Apache