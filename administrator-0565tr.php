    <!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Table</title>
    
    <meta name="description" content="Free Admin Template Based On Twitter Bootstrap 3.x">
    <meta name="author" content="">
    
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/admin/img/metis-tile.png" />
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/admin/lib/bootstrap/css/bootstrap.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/admin/lib/font-awesome/css/font-awesome.css">
    
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="assets/admin/css/main.css">
    
    <!-- metisMenu stylesheet -->
    <link rel="stylesheet" href="assets/admin/lib/metismenu/metisMenu.css">
    
    <!-- onoffcanvas stylesheet -->
    <link rel="stylesheet" href="assets/admin/lib/onoffcanvas/onoffcanvas.css">
    
    <!-- animate.css stylesheet -->
    <link rel="stylesheet" href="assets/admin/lib/animate.css/animate.css">


        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

    <!--For Development Only. Not required -->
    <script>
        less = {
            env: "development",
            relativeUrls: false,
            rootpath: "/assets/admin/"
        };
    </script>
    <link rel="stylesheet" href="assets/admin/css/style-switcher.css">
    <link rel="stylesheet/less" type="text/css" href="assets/admin/less/theme.less">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.1/less.js"></script>

  </head>

        <body class="  ">
		<div class="bg-dark dk" id="wrap">
                    <div id="top">
                    <!-- .navbar -->
                    <nav class="navbar navbar-inverse navbar-static-top">
                        <div class="container-fluid">
                    
                    
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <header class="navbar-header">
                    
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a href="index.php" class="navbar-brand"><img src="assets/admin/img/logo.png" alt=""></a>
                    
                            </header>
                    
                    
                    
                            <div class="topnav">
                                
                    
                                <!-- .nav -->
                                <ul class="nav navbar-nav">
									<li><a href="deconnexion.php" onclick="return(confirm('Voulez-vous vraiment vous déconnecter ?'));">Deconnexion</a></li>
                                </ul>
                                <!-- /.nav -->
                           
                            </div>


                    
			<div class="collapse navbar-collapse navbar-ex1-collapse">
	
			</div>
		</div>
		<!-- /.container-fluid -->
	</nav>
	<!-- /.navbar -->
		<header class="head">
				
				<!-- /.search-bar -->
			<div class="main-bar">
				<h3>
              <i class="fa fa-table"></i>&nbsp;
            
          </h3>
                            </div>
                            <!-- /.main-bar -->
                        </header>
                        <!-- /.head -->
                </div>
				</div>
                <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">
                            <!--Begin Datatables-->
<div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Authentification requise - Accès réservé aux administrateurs du site</h5>
                
            </header>
			<div class="grid_6 prefix_1">
            <div id="collapse4" class="body">
				<form action="administrator-0565tr/dashboard" method="post" class="form-horizontal">
                <?php if(ISSET($_GET['error'])) echo 'Pseudo ou mot de passe incorrect(s)' ; ?>
                <div class="form-group">
					<label>Email</label>
					<input name="pseudo" type="text" placeholder="Votre pseudo" required="required" /><br />
				</div>
				<div class="form-group">
					<label>Mot de Passe</label>
					<input name="pwd" type="password" placeholder="Votre mot de passe" required="required" /><br />
				</div>
					<input type="submit" value="Connexion" class="btn btn-primary"/>
				</form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<!--End Datatables-->




                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.outer -->
                </div>
                <!-- /#content -->
                        <!-- /.well well-small -->
                        <!-- .well well-small -->
             <!-- /#wrap -->
<?php
	//include 'application\views\templates\admin\footer.php';
?>	



