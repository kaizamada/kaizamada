<div class="content_area">
      <div class="main_content floatleft">
        <div class="left_coloum floatleft">
        
          <div class="single_left_coloum_wrapper">
<h2 class="title"><?= $categorie_single->nomCategorie ?> <?php if($sous_categorie_exist) { ?>► <?= $categorie_single->nomSousCategorie ?> <?php } ?></h2>
            
            <?php foreach ($articles AS $article) {  ?>
                    
            <div class="single_left_coloum floatleft"> <img src="<?= base_url() ?>assets/images/thumbnail/<?= $article->id ?>_thumb.jpg" alt="" />
              <h3><?= $article->titre ?></h3>
              <a class="readmore" href="<?= base_url() ?>article/<?= $article->titre ?>-<?= $article->id ?>">Lire l'article</a> 
            </div>
            <?php  } ?>
            
          </div>
          
          
        </div>
      </div>
      <div class="sidebar floatright">
        
        <div class="single_sidebar">
          <h2 class="title">PUBLICITE</h2>
          <ul>
            <li class="publicite"></li>
          </ul> 
        </div>
      </div>
    </div>