
                <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">
                            <!--Begin Datatables-->
<div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>

			<h5>Ajouter un nouvel article</h5>
        </header>
            <div id="collapse4" class="body">
			
			<form action="<?= base_url() ?>administrator-0565tr/article/add_post" method="post" class="form-horizontal" enctype="multipart/form-data">
				<div class="form-group">
					<label class="control-label col-lg-4">Titre :</label>
					<div class="col-lg-4">
						<input type="text" class="form-control" required name="titre" id="titre" placeholder="Titre de l'article"/>
					</div>
					<br/>
                </div>
                <div class="form-group">
                <label class="control-label col-lg-4">Catégorie :</label>
					<div class="col-lg-4">
                    <select class="form-control chzn-select" name="categorie">
                    <?php foreach ($categories AS $categorie) {   ?>	
                        <option value="<?= $categorie->idCategorie ?>-<?= $categorie->idSousCategorie ?>"><?= $categorie->nomCategorie ?> - <?= $categorie->nomSousCategorie ?></option>
                    <?php } ?>
                    </select>
                    </div>
					<br/>
                </div>
                
                <div class="form-group">
					<label class="control-label col-lg-4">Texte :</label>
					<div class="col-lg-4">
						<textarea type="text" class="form-control" required name="texte" id="texte"></textarea>
					</div>
					<br/>
                </div>
                <div class="form-group">
					<label class="control-label col-lg-4">Auteur :</label>
					<div class="col-lg-4">
						<input type="text" class="form-control" required name="auteur" id="auteur" placeholder="Auteur de l'article"/>
					</div>
					<br/>
                </div>
				<div class="form-group">	
					<label class="control-label col-lg-4">Image d'illustration:</label>
					<div class="col-lg-4"><input type="file" class="form-control" required name="image" id="image"/></div>
				</div>
				<br/>
				<div class="form-group col-lg-4">	
					<div class="form-actions no-margin-bottom col-lg-4">
						<input type="submit" value="Ajouter" class="btn btn-primary"/>
					</div>
				</div>
            </form>
            </div>
				</div>
			</div>
			</div>
			</div>
			<!-- /.inner -->
		</div>
		<!-- /.outer -->
	</div>
	<!-- /#content -->
			<!-- /.well well-small -->
			<!-- .well well-small -->
 <!-- /#wrap -->