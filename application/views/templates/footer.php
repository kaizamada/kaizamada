<div class="footer_top_area">
      <div class="footer_bottom_area">
        <div class="footer_menu">
          
        </div>
    </div>
    
      <div class="copyright_text">
        <p>Copyright &copy; 2018 | RAKOTOARISOA Andriniaina Tefy - Étudiant à l'IT-University Andoharanofotsy Madagascar (Promotion 9 - Groupe B - Nº20)| Design by <a target="_blank" rel="nofollow" href="http://www.graphicsfuel.com/2045/10/wp-magazine-theme-template-psd/">Rafi MD</a></p>
        <p>Les marques et photos sont sous le Copyright de leurs propriétaires respectifs et ne sont utilisés ici que dans le cadre d'un projet universitaire.</p>
        <h3 id="avertissement">Avertissement</h3>
        <p>Les articles publiés sur ce site sont entièrement <strong>faux</strong> et ne servent qu'à célébrer le traditionnel "Poisson d'Avril". Nous ne nous tenons donc pas responsables d'éventuelles propagations de fausses nouvelles ou d'émeutes publiques qui en découlent.</p>
        <p>Merci de votre compréhension</p>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-min.js"></script> 
<script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.bxslider.js"></script> 
<script type="text/javascript" src="<?= base_url() ?>assets/js/selectnav.min.js"></script> 
<script type="text/javascript">
selectnav('nav', {
    label: '-Navigation-',
    nested: true,
    indent: '-'
});
selectnav('f_menu', {
    label: '-Navigation-',
    nested: true,
    indent: '-'
});
$('.bxslider').bxSlider({
    mode: 'fade',
    captions: true
});
</script>
</body>
</html>