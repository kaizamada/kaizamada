<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?= $title ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="Description" content="K'AIZA MADA, le site d'actualités entièrement dédié à la Grande Île, à savoir Madagascar. Infos : politique, économie, société...">
<meta name="Author" content="Tefy Rakotoarisoa">
<meta name="Keywords" content="Actualités, Infos, Madagascar, Grande Île, Politique, Économie, Société">
<meta name="Robots" content="noodp">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/font/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/font/font.css" />
<!--<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/bootstrap.min.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/responsive.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/jquery.bxslider.css" media="screen" />
-->
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/all.css" media="screen" />
</head>
<body>
<div class="body_wrapper">
  <div class="center">
    <div class="header_area">
      <div class="logo floatleft"><a id="linklogo" href="<?= base_url() ?>"><img src="<?= base_url() ?>assets/images/logo.png" alt="" /></a></div>
      <div class="top_menu floatleft">
        <em>Le <strong>site d'actualités</strong> entièrement dédié à la <strong>Grande Île</strong></em>
      </div>
      <div class="social_plus_search floatright">
        <div class="social">
          <ul>
            <li><a href="#" class="twitter"></a></li>
            <li><a href="#" class="facebook"></a></li>
            <li><a href="#" class="feed"></a></li>
          </ul>
        </div>
        
      </div>
    </div>
    <div class="main_menu_area">
      <ul id="nav">
        <li><a href="<?= base_url() ?>">Infos du jour</a><li>
        <?php foreach ($categories AS $categorie) {   ?>
        <li><a href="<?= base_url() ?>categorie/<?= $categorie->id ?>"><?= $categorie->nom ?></a>
            <?php foreach ($sous_categories AS $sous_categorie) { 
            if($sous_categorie->idCategorie==$categorie->id) {   ?>
            <ul>
                <?php foreach($sous_categories AS $sous_categorie) { ?>
                <li><a href="<?= base_url() ?>categorie/<?= $categorie->id ?>/<?= $sous_categorie->id ?>"><?= $sous_categorie->nom ?></a></li>
                <?php } ?>
            </ul>
            <?php break; } } ?>
        </li>
        <?php } ?>
        
      </ul>
    </div>