<div class="slider_area">
      <div class="slider">
        <ul class="bxslider">
        <?php for($i=0; $i<3; $i++) {  ?>
          <li><a href="<?= base_url() ?>article/<?= $articles[$i]->titre ?>-<?= $articles[$i]->id ?>"><img src="<?= base_url() ?>assets/images/<?= $articles[$i]->id ?>.jpg" alt="" title="<?= substr($articles[$i]->titre, 0, 95) ?>..." /></a></li>
        <?php } ?>
          
        </ul>
      </div>
    </div>
<div class="content_area">
      <div class="main_content floatleft">
        <div class="left_coloum floatleft">
        <?php foreach ($categories AS $categorie) {   ?>
          <div class="single_left_coloum_wrapper">
            <h2 class="title"><?= $categorie->nom ?></h2>
            <a class="more" href="<?= base_url() ?>categorie/<?= $categorie->id ?>">Plus d'articles</a>
            <?php foreach ($articles AS $article) {  
                    if($categorie->id==$article->idCategorie) { ?>
            <div class="single_left_coloum floatleft"> <img src="<?= base_url() ?>assets/images/thumbnail/<?= $article->id ?>_thumb.jpg" alt="<?= $article->titre ?>" />
              <h3><?= $article->titre ?></h3>
              <a class="readmore" href="<?= base_url() ?>article/<?= $article->titre ?>-<?= $article->id ?>">Lire l'article</a> 
            </div>
            <?php } } ?>
            
          </div>
          <?php } ?>
          
        </div>
      </div>
      <div class="sidebar floatright">
        
        <div class="single_sidebar">
          <h2 class="title">PUBLICITE</h2>
          <ul>
            <li class="publicite"></li>
          </ul> 
        </div>
      </div>
    </div>