<div class="content_area">
      <div class="main_content floatleft">
        <div class="left_coloum floatleft">
        
          <div class="single_left_coloum_wrapper">
            <h1 class="title-single"><?= $article_single->titre ?></h1>
            <div>
            <h2><a class="category_single" href="<?= base_url() ?>categorie/<?= $article_single->idCategorie ?>"><?= $article_single->nomCategorie ?></a></h2>
            <?php if($article_single->idSousCategorie!=1) { ?>
            <h3><a class="souscategory_single" href="<?= base_url() ?>categorie/<?= $article_single->idCategorie ?>/<?= $article_single->idSousCategorie ?>"><?= $article_single->nomSousCategorie ?></a></h3>
            <?php } ?>
            </div>
            <p class="text-single"><em>Publié le <?= $article_single->daty  ?></em></p>
            <div class="photo_single"> <img src="<?= base_url() ?>assets/images/<?= $article_single->id  ?>.jpg" alt="<?= $article_single->titre  ?>" /></div>
            <p class="text-single"><?= $article_single->texte ?></p>
            <p class="auteur-single">♦ Par <?= $article_single->auteur ?></p>
            
            
            
          </div>
         
          
        </div>
      </div>
      <div class="sidebar floatright">
        
        <div class="single_sidebar">
          <h2 class="title">PUBLICITE</h2>
          <ul>
            <li class="publicite"></li>
          </ul> 
        </div>
      </div>
    </div>