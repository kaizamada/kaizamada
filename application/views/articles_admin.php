
                <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter">
                            <!--Begin Datatables-->
<div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Gestion des articles</h5>
            </header>
            <div id="collapse4" class="body">
			<p><strong><a href="<?= base_url() ?>administrator-0565tr/article/add" >+ Ajouter un nouvel article</a></strong></p>
                <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Titre</th>
                        <th>Image</th>
                        <th>Catégorie</th>
                        <th>Sous-catégorie</th>
                        <th>Auteur</th>
                        <th>Date de publication</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
						<?php foreach($articles AS $article){ ?>
                            <tr>
                                <td><a href="#"><?= $article->id ?></a></td>
                                <td><?= $article->titre ?></td>
                                <td><img src="<?= base_url() ?>assets/images/thumbnail/<?= $article->id ?>_thumb.jpg" width="80" /></td>
                                <td><?= $article->nomCategorie ?></td>
                                <td><?= $article->nomSousCategorie ?></td>
                                <td><?= $article->auteur ?></td>
                                <td><?= $article->daty ?></td>
                                <th><a href="<?= base_url() ?>administrator-0565tr/article/delete/<?= $article->id ?>" onclick="return(confirm('Voulez-vous vraiment supprimer ce article ?'));">Supprimer</a></th>
                            </tr>
                            <?php } ?>
                            
                    </tbody>                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<!--End Datatables-->




                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.outer -->
                </div>
                <!-- /#content -->
                        <!-- /.well well-small -->
                        <!-- .well well-small -->
             <!-- /#wrap -->