<?php
    class Article_model extends CI_Model{
        function __construct(){
            parent::__construct();
        }
        public function insert($dataArticle){
            if ($this->db->insert("Article", $dataArticle)) {
                return true;
            }
        }
        public function insertDetail($dataArticleDetail){
            if ($this->db->insert("ArticleDetail", $dataArticleDetail)) {
                return true;
            }
        }
        public function delete($idArticle){
            if ($this->db->delete("ArticleDetail", "idArticle = ".$idArticle)) {
                if ($this->db->delete("Article", "id = ".$idArticle)) {
                    return true;
                }
            }
        }
        public function update($data,$idArticle){
            $this->db->set($data);
            $this->db->where("id", $idArticle);
            $this->db->update("Article", $data);
        }
        public function getOne($id) {
            $query = $this->db->get_where("ArticleView",array("id"=>$id));
            return $query->result()[0];
        }
        public function getByCategorie($idCategorie, $idSousCategorie=1) {
            if($idSousCategorie==1)
                $query = $this->db->get_where("Article",array("idCategorie"=>$idCategorie));
            else
                $query = $this->db->get_where("Article",array("idCategorie"=>$idCategorie, "idSousCategorie"=>$idSousCategorie));
            return $query->result();
        }
    }
?>