<?php
    class Categorie_model extends CI_Model{
        function __construct(){
            parent::__construct();
        }
        public function insert($data){
            if ($this->db->insert("Categorie", $data)) {
                return true;
            }
        }
        public function delete($idCategorie){
            if ($this->db->delete("Categorie", "id = ".$idCategorie)) {
                return true;
            }
        }
        public function update($data,$idCategorie){
            $this->db->set($data);
            $this->db->where("id", $idCategorie);
            $this->db->update("Categorie", $data);
        }
        public function get_sous_categorie() {
            $query = $this->db->where("idCategorie>1")->get('SousCategorie');
            return $query->result();
        }
        public function get_name_categorie_complet($idCategorie, $idSousCategorie=1) {
            if($idSousCategorie==1)
                $query =  $this->db->get_where("CategorieView",array("idCategorie"=>$idCategorie));
            else
                $query = $this->db->get_where("CategorieView",array("idCategorie"=>$idCategorie, "idSousCategorie"=>$idSousCategorie));
            return $query->result()[0];
        }
        
    }
?>