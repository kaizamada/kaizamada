<?php 
class Categorie_controller extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
    }
    public function get_souscategorie($idCategorieMere) {
        $query = $this->db->get_where("SousCategorie",array("idCategorie"=>$idCategorieMere));
        return $query->result();
    }

}
?>