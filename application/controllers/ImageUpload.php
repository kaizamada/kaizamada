<?php
class ImageUpload extends CI_Controller {
   /**
    * Manage __construct
    *
    * @return Response
   */
   public function __construct() { 
      parent::__construct(); 
      $this->load->helper(array('form', 'url')); 
   }
   /**
    * Manage index
    *
    * @return Response
   */
   public function index() { 
      $this->load->view('imageUploadForm', array('error' => '' )); 
   } 

   /**
    * Manage uploadImage
    *
    * @return Response
   */
   public function uploadImage() { 

      $config['upload_path']   = './assets/images'; 
      $config['file_name']   = 'lala'; 
      $config['allowed_types'] = 'jpg'; 
      $config['max_size']      = 2048;
      $this->load->library('upload', $config);

      if ( ! $this->upload->do_upload('image')) {
         $error = array('error' => $this->upload->display_errors()); 
         $this->load->view('imageUploadForm', $error); 
      }else { 
        $uploadedImage = $this->upload->data();
        $this->resizeImage($uploadedImage['file_name']);
        print_r('Image Uploaded Successfully.');
        exit;
      } 
   }
   /**
    * Manage uploadImage
    *
    * @return Response
   */
   public function resizeImage($filename)
   {
      $source_path =  './assets/images/' . $filename;
      $target_path =  './assets/images/thumbnail';
      $config_manip = array(
     
          'image_library' => 'gd2',
          'source_image' => $source_path,
          'new_image' => $target_path,
          'maintain_ratio' => FALSE,
          'create_thumb' => TRUE,
          'thumb_marker' => '_thumb',
          'x_axis' => '150',
          'y_axis' => '150',
          'width' => 150,
          'height' => 150
      );

      $this->load->library('image_lib', $config_manip);
      if (!$this->image_lib->crop()) {
            echo 'Erreur de resizing ' . $_SERVER['DOCUMENT_ROOT'];
          echo $this->image_lib->display_errors();
          

      }

      $this->image_lib->clear();

   }

} 

?>