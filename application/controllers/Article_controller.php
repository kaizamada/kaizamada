<?php 
class Article_controller extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url', 'form');
        $this->load->database();
        $this->load->model('Categorie_model');
        $this->load->model('Article_model');
    }
    public function index(){
        $queryCategorie = $this->db->get("Categorie");
        $querySousCategorie = $this->Categorie_model->get_sous_categorie();
        $query = $this->db->get("Article");
        $data['title'] = "K'AIZA MADA - Les actualités de Madagascar";
        $data['categories'] = $queryCategorie->result();
        $data['sous_categories'] = $querySousCategorie;
        $data['articles'] = $query->result();
        $data['contents'] = 'accueil';    
        $this->load->helper('url');
        $this->load->view('templates/template',$data);
    }
    public function show_single($idArticle){
        $queryCategorie = $this->db->get("Categorie");
        $querySousCategorie = $this->Categorie_model->get_sous_categorie();
              
        $data['categories'] = $queryCategorie->result();
        $data['sous_categories'] = $querySousCategorie;
        $data['article_single'] = $this->Article_model->getOne($idArticle);
        $data['title'] = $data['article_single']->titre . " - K'AIZA MADA";
        $data['contents'] = 'article';    
        $this->load->helper('url');
        $this->load->view('templates/template',$data);
    }
    public function show_per_categorie($idCategorie, $idSousCategorie=1){
        $queryCategorie = $this->db->get("Categorie");
        $querySousCategorie = $this->Categorie_model->get_sous_categorie();
        $data['categorie_single'] = $this->Categorie_model->get_name_categorie_complet($idCategorie, $idSousCategorie);
        $data['categories'] = $queryCategorie->result();
        $data['sous_categories'] = $querySousCategorie;
        $data['sous_categorie_exist'] = true;
        if($idSousCategorie==1)
            $data['sous_categorie_exist'] = false;
        $data['articles'] =$this->Article_model->getByCategorie($idCategorie, $idSousCategorie);
        $data['title'] = $data['categorie_single']->nomCategorie . " - K'AIZA MADA";
        if($data['sous_categorie_exist'])
        $data['title'] = $data['categorie_single']->nomCategorie . " - " . $data['categorie_single']->nomSousCategorie . " - K'AIZA MADA";
        $data['contents'] = 'articles_per_categorie';    
        $this->load->helper('url');
        $this->load->view('templates/template',$data);
    }
    public function show_admin(){
        if($this->session->userdata('Administrateur')==null)
            redirect('administrator-0565tr.php');
        else{
            $query = $this->db->get("ArticleTextLessView");
            $data['title'] = "Admin - Articles - K'AIZA MADA - Les actualités de Madagascar";
            $data['articles'] = $query->result();
            $data['contents'] = 'articles_admin';    
            $this->load->helper('url');
            $this->load->view('templates/admin/template',$data);
        }
    }
    public function add_form_admin(){
        if($this->session->userdata('Administrateur')==null)
            redirect('administrator-0565tr.php');
        else{
            $query = $this->db->get("CategorieView");
            $data['title'] = "Admin - Ajout Article - K'AIZA MADA - Les actualités de Madagascar";
            $data['categories'] = $query->result();
            $data['contents'] = 'ajouter_article_admin';    
            $this->load->helper('url');
            $this->load->view('templates/admin/template',$data);
        }
    }
    public function add_admin(){
        if($this->session->userdata('Administrateur')==null)
            redirect('administrator-0565tr.php');
        else{
            
            
            $idCategorie = explode('-', $this->input->post('categorie'));
            if($idCategorie[1]==null)
                $idCategorie[1]='1';
            $dataArticle = array(
                'idCategorie' => $idCategorie[0],
                'idSousCategorie' => $idCategorie[1],
                'titre' => $this->input->post('titre'),
                'image' => $this->input->post('titre'),
                'daty' => date("Y-m-d")
            );
            $this->Article_model->insert($dataArticle);
            $query = $this->db->order_by('id', 'DESC')->get_where("Article");
            $idArticle = $query->result()[0];
            $dataArticleDetail = array(
                'idArticle' => $idArticle->id,
                'texte' => $this->input->post('texte'),
                'auteur' => $this->input->post('auteur')
            );
            $this->Article_model->insertDetail($dataArticleDetail);
            $this->uploadImage($idArticle->id);
            $this->show_admin();
        }
        
    }
    public function delete_admin($idArticle){
        if($this->session->userdata('Administrateur')==null)
            redirect('administrator-0565tr.php');
        else{
            $this->Article_model->delete($idArticle);
            $this->show_admin();
        }
    }
    public function uploadImage($image_name) { 

        $config['upload_path']   = './assets/images'; 
        $config['file_name']   = $image_name; 
        $config['allowed_types'] = 'jpg'; 
        $config['max_size']      = 2048;
        $this->load->library('upload', $config);
  
        if ( ! $this->upload->do_upload('image')) {
           $error = array('error' => $this->upload->display_errors()); 
           $this->load->view('imageUploadForm', $error); 
        }else { 
          $uploadedImage = $this->upload->data();
          $this->resizeImage($uploadedImage['file_name']);
          //print_r('Image Uploaded Successfully.');
          //exit;
        } 
     }
     /**
      * Manage uploadImage
      *
      * @return Response
     */
     public function resizeImage($filename)
     {
        $source_path =  './assets/images/' . $filename;
        $target_path =  './assets/images/thumbnail';
        $config_manip = array(
       
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => FALSE,
            'create_thumb' => TRUE,
            'thumb_marker' => '_thumb',
            'x_axis' => '110',
            'y_axis' => '110',
            'width' => 150,
            'height' => 150
        );
  
        $this->load->library('image_lib', $config_manip);
        if (!$this->image_lib->resize()) {
              echo 'Erreur de resizing ' . $_SERVER['DOCUMENT_ROOT'];
            echo $this->image_lib->display_errors();
            
        }
       /* if (!$this->image_lib->crop()) {
            echo 'Erreur de cropping ' . $_SERVER['DOCUMENT_ROOT'];
          echo $this->image_lib->display_errors();
          
      }*/
  
        $this->image_lib->clear();
  
     }
    
    

}
?>