<?php
class Admin_Login_controller extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
    }
    public function index(){
        
        $pseudo = $this->input->post('pseudo');
        $pwd = $this->input->post('pwd');
        $query = $this->db->get_where("Administrateur",array("pseudo"=>$pseudo, "pwd"=>sha1($pwd)));
        $infoUser = $query->result();
        $valider = ($infoUser != NULL);
        if ($valider == FALSE) {
            $data['msg'] = "Erreur";
            redirect('administrator-0565tr.php?error=1', 'refresh');
        }
        else if($valider == TRUE){
            $this->session->set_userdata('Administrateur', $pseudo);
            
            //$query = $this->db->get("produit");
            //$data['produits'] = $query->result();
            //$data['contents'] = 'accueil';    
            //$this->load->view('templates/template',$data);
            redirect('administrator-0565tr/articles', 'refresh');
           
        }
    }
}