--MySQL
CREATE DATABASE kaizamada

CREATE TABLE Categorie (
	id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
	nom VARCHAR(25),
	rang TINYINT UNSIGNED,
	PRIMARY KEY (id)
)Engine=InnoDB;

CREATE TABLE SousCategorie (
	id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    idCategorie TINYINT UNSIGNED,
	nom VARCHAR(25),
	rang TINYINT UNSIGNED,
	PRIMARY KEY (id),
    FOREIGN KEY(idCategorie) REFERENCES Categorie(id)
)Engine=InnoDB;

CREATE TABLE Article (
	id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    idCategorie TINYINT UNSIGNED,
    idSousCategorie TINYINT UNSIGNED,
	titre VARCHAR(150),
    image VARCHAR(200),
    daty DATE,
	PRIMARY KEY (id),
    FOREIGN KEY(idCategorie) REFERENCES Categorie(id),
    FOREIGN KEY(idSousCategorie) REFERENCES SousCategorie(id)
)Engine=InnoDB;

CREATE TABLE ArticleDetail (
	id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    idArticle TINYINT UNSIGNED,
	texte TEXT(2000),
    auteur VARCHAR(100),
	PRIMARY KEY (id),
    FOREIGN KEY(idArticle) REFERENCES Article(id)
)Engine=InnoDB;

INSERT INTO Categorie(nom, rang) values
('Politique', 1),
('Societe', 2),
('Economie', 3);

INSERT INTO SousCategorie(idCategorie, nom, rang) values
(1, 'Aucune', 0),
(2, 'Sante', 1),
(2, 'Sport', 2);

INSERT INTO Article(idCategorie, idSousCategorie, titre, image, daty) values
(1, 1, 'Présidentielles 2018: Justin Bieber soutiendra Hery Rajaonarimampianina lors de sa campagne', 'présidentielles-2018-justin-bieber-soutiendra-hery-rajaonarimampianina-lors-de-sa-campagne', '2018-04-01'),
(3, 1, 'Une nouvelle espèce de lémurien découverte dans le massif du Makay !', 'une-nouvelle-espèce-de-lémurien-découverte-dans-le-massif-du-makay', '2018-03-31'),
(2, 2, 'Des centaines de chats importés d’Europe pour lutter contre la Peste à Madagascar', 'des-centaines-de-chats-importés-d-europe-pour-lutter-contre-la-peste-a-madagascar', '2018-03-25');

INSERT INTO ArticleDetail(idArticle, texte, auteur) values
(1, 'À Madagascar, en politique, tous les coups sont permis. Et pour les candidats aux futures élections présidentielles, tous les moyens sont bons pour mettre un maximum d’électeurs dans leurs poches. Hery Rajaonarimampianina ne fait pas exception à la règle, qui en plus n’hésite pas à employer les grands moyens. C’est pourquoi il a demandé à la star internationale Justin Bieber de le soutenir durant les campagnes de propagande. « J’ai beaucoup aimé son nouveau programme et je suis sûr qu’il sera le meilleur dirigeant que ce pays aura connu. » a confirmé le chanteur sur son compte Twitter. Il promet également d’écrire et de chanter une chanson pour promouvoir son candidat préféré. Enfin, ce dernier, à savoir Hery Rajaonarimampianina, a imposé une dernière condition pour être sûr de remporter ces élections. Le Président déclare : « Oui, Justin Bieber viendra chanter chez nous, mais seulement après la proclamation des résultats de l’élection, et SEULEMENT SI JE LA GAGNE ! ».', 'Hervé Ramangamananasy'),
(2, 'C’est l’un des lieux les moins connus de Madagascar. Situé dans le Sud-Est de l’Île, le massif du Makay est un peu comme la forêt amazonienne malgache, encore peu exploré par l’homme. C’est pourquoi ce drôle de lémurien à la queue multicolore n’a été repérée que cette année. Découvert par l’explorateur américain Jay Troovey au début du mois de mars 2018, au cœur de la forêt du massif. Il ressemble au Maki Catta, sauf que sa queue d’ordinaire noire et blanche, est pourvue ici de toutes les couleurs de l’arc-en-ciel. L’explorateur n’a pour l’instant repéré qu’un spécimen, mais il espère vite trouver le territoire de la famille au complet ! On croise les doigts…', 'R.A. Jacques'),
(3, 'Ne riez pas ! Ceci est très sérieux. Après tout, les chats capturent les rongeurs pour les dévorer, non ? Mais se débarrasser des rats est-elle une solution efficace pour en finir avec la peste ? Qu’en est-il aussi de l’hygiène, des ordures, et des autres vecteurs de la peste (puces, etc…) ? En fait, selon le spécialiste en épidémiologie, Dr. Tom Hégéri, je cite : « Le rat est un des vecteurs indispensables à la propagation de la peste. En supprimant ce vecteur, nous brisons la chaîne, ce qui annule le cycle de la saison pesteuse. ». D’accord, mais des chats peuvent-ils alors éradiquer à jamais ces rats porteurs de la peste ? Eh bien, oui ! Il semblerait que ce félin domestique a déjà fait ses preuves une fois en Australie. Introduit par les européens au XXème siècle, le chat a eu raison d’un petit rongeur endémique australien : le bandicoot. Il n’en subsiste que très peu aujourd’hui. Un regret pour cette espèce protégée, mais heureusement, nos rats ne le sont pas, et nous pourrons les éradiquer légalement. Le ministère de la santé a d’ores et déjà commandé 500 chats domestiques qui devraient débarquer vers début Mai.', 'Pissou Kelly');

CREATE VIEW ArticleView AS 
SELECT a.*, c.nom as nomCategorie, sc.nom as nomSousCategorie, ad.texte, ad.auteur
FROM Article a
JOIN Categorie c ON a.idCategorie = c.id
JOIN SousCategorie sc ON a.idSousCategorie = sc.id
JOIN ArticleDetail ad ON a.id = ad.idArticle;

CREATE VIEW CategorieView AS 
SELECT c.id as idCategorie, c.nom as nomCategorie, sc.id as idSousCategorie, sc.nom as nomSousCategorie
FROM Categorie c
LEFT JOIN SousCategorie sc ON c.id = sc.idCategorie;

CREATE TABLE Administrateur (
	id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
    pseudo VARCHAR(100),
	pwd VARCHAR(250),
	PRIMARY KEY (id)
)Engine=InnoDB;

INSERT INTO Administrateur(pseudo, pwd) VALUES
('Admin', sha1('root1234'));

CREATE VIEW ArticleTextLessView AS 
SELECT a.*, c.nom as nomCategorie, sc.nom as nomSousCategorie, ad.auteur
FROM Article a
JOIN Categorie c ON a.idCategorie = c.id
JOIN SousCategorie sc ON a.idSousCategorie = sc.id
JOIN ArticleDetail ad ON a.id = ad.idArticle;